#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# This program is part of GASP, an immediate mode graphics library and
# accompanying educational resources for beginning Programmers using Python.
# Copyright (C) 2020, the GASP Development Team
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

"""
color.py : Constants for colors (RBG values)
"""

# fmt: off

ALICEBLUE            = (240, 248, 255)  # F0 F8 FF
ANTIQUEWHITE         = (250, 235, 215)  # FA EB D7
AQUA                 = (  0, 255, 255)  # 00 FF FF
AZURE                = (240, 255, 212)  # F0 FF D4
BEIGE                = (245, 245, 220)  # F5 F5 DC
BISQUE               = (245, 245, 220)  # F5 F5 DC
BLACK                = (  0,   0,   0)  # 00 00 00
BLANCHEDALMOND       = (255, 255, 205)  # FF FF CD
BLUE                 = (  0,   0, 255)  # 00 00 FF
BLUEVIOLET           = (138,  43, 226)  # 8A 2B E2
BROWN                = (165,  42,  42)  # A5 2A 2A
BURLYWOOD            = (222, 184, 135)  # DE B8 87
CADETBLUE            = ( 95, 158, 160)  # 5F 9E A0
CHARTREUSE           = (127, 255,   0)  # 7F FF 00
CHOCOLATE            = (210, 105,  30)  # D2 69 1E
CORAL                = (255, 127,  80)  # FF 7F 50
CORNFLOWERBLUE       = (100, 149, 237)  # 64 95 ED
CORNSILK             = (255, 248, 220)  # FF F8 DC
CRIMSON              = (220,  20,  60)  # DC 14 3C
CYAN                 = (  0, 255, 255)  # 00 FF FF
DARKBLUE             = (  0,   0, 139)  # 00 00 8B
DARKCYAN             = (  0, 139, 139)  # 00 8B 8B
DARKGOLDENROD        = (184, 134,  11)  # B8 86 0B
DARKGRAY             = (169, 169, 169)  # A9 A9 A9
DARKGREEN            = (  0, 100,   0)  # 00 64 00
DARKKHAKI            = (189, 183, 107)  # BD B7 6B
DARKMAGENTA          = (139,   0, 139)  # 8B 00 8B
DARKOLIVEGREEN       = ( 85, 107,  47)  # 55 6B 2F
DARKORANGE           = (255, 140,   0)  # FF 8C 00
DARKORCHID           = (155,  50, 204)  # 9B 32 CC
DARKRED              = (139,   0,   0)  # 8B 00 00
DARKSALMON           = (233, 150, 122)  # E9 96 7A
DARKSEAGREEN         = (143, 188, 143)  # 8F BC 8F
DARKSLATEBLUE        = ( 72,  61, 139)  # 48 3D 8B
DARKSLATEGRAY        = ( 47,  79,  79)  # 2F 4F 4F
DARKTURQUOISE        = (  0, 206, 209)  # 00 CE D1
DARKVIOLET           = (148,   0, 211)  # 94 00 D3
DEEPPINK             = (255,  20, 147)  # FF 14 93
DEEPSKYBLUE          = (  0, 191, 255)  # 00 BF FF
DIMGRAY              = (105, 105, 105)  # 69 69 69
DODGERBLUE           = ( 30, 144, 255)  # 1E 90 FF
FIREBRICK            = (178,  34,  34)  # B2 22 22
FLORALWHITE          = (255, 250, 240)  # FF FA F0
FORESTGREEN          = ( 34, 139,  34)  # 22 8B 22
FUCHSIA              = (255,   0, 255)  # FF 00 FF
GAINSBORO            = (220, 220, 220)  # DC DC DC
GHOSTWHITE           = (248, 248, 255)  # F8 F8 FF
GOLD                 = (255, 215,   0)  # FF D7 00
GOLDENROD            = (218, 165,  32)  # DA A5 20
GRAY                 = (127, 127, 127)  # 7F 7F 7F
GREEN                = (  0, 128,   0)  # 00 80 00
GREENYELLOW          = (173, 255,  47)  # AD FF 2F
HONEYDEW             = (240, 255, 240)  # F0 FF F0
HOTPINK              = (255, 105, 180)  # FF 69 B4
INDIANRED            = (205,  92,  92)  # CD 5C 5C
INDIGO               = ( 75,   0, 130)  # 4B 00 82
IVORY                = (255, 240, 240)  # FF F0 F0
KHAKI                = (240, 230, 140)  # F0 E6 8C
LAVENDER             = (230, 230, 250)  # E6 E6 FA
LAVENDERBLUSH        = (255, 240, 245)  # FF F0 F5
LAWNGREEN            = (124, 252, 245)  # 7C FC F5
LEMONCHIFFON         = (255, 250, 205)  # FF FA CD
LIGHTBLUE            = (173, 216, 230)  # AD D8 E6
LIGHTCORAL           = (240, 128, 128)  # F0 80 80
LIGHTCYAN            = (224, 255, 255)  # E0 FF FF
LIGHTGOLDENRODYELLOW = (250, 250, 210)  # FA FA D2
LIGHTGREEN           = (144, 238, 144)  # 90 EE 90
LIGHTGRAY            = (211, 211, 211)  # D3 D3 D3
LIGHTPINK            = (255, 182, 193)  # FF B6 C1
LIGHTSALMON          = (255, 160, 122)  # FF A0 7A
LIGHTSEAGREEN        = ( 32, 178, 170)  # 20 B2 AA
LIGHTSKYBLUE         = (135, 206, 250)  # 87 CE FA
LIGHTSLATEGRAY       = (119, 136, 153)  # 77 88 99
LIGHTSTEELBLUE       = (176, 196, 222)  # B0 C4 DE
LIGHTYELLOW          = (255, 255, 224)  # FF FF E0
LIME                 = (  0, 255,   0)  # 00 FF 00
LIMEGREEN            = ( 50, 205,  50)  # 32 CD 32
LINEN                = (250, 240, 230)  # FA F0 E6
MAGENTA              = (255,   0, 255)  # FF 00 FF
MAROON               = (128,   0,   0)  # 80 00 00
MEDIUMAQUAMARINE     = (102, 205, 170)  # 66 CD AA
MEDIUMBLUE           = (  0,   0, 205)  # 00 00 CD
MEDIUMORCHID         = (186,  85, 211)  # BA 55 D3
MEDIUMPURPLE         = (147, 112, 219)  # 93 70 DB
MEDIUMSEAGREEN       = ( 60, 179, 113)  # 3C B3 71
MEDIUMSLATEBLUE      = (123, 104, 238)  # 7B 68 EE
MEDIUMSPRINGGREEN    = (  0, 250, 154)  # 00 FA 9A
MEDIUMTURQOISE       = ( 72, 209, 204)  # 48 D1 CC
MEDIUMVIOLETRED      = (199,  21, 133)  # C7 15 85
MIDNIGHTBLUE         = ( 25,  25, 112)  # 19 19 70
MINTCREAM            = (245, 255, 250)  # F5 FF FA
MISTYROSE            = (255, 228, 225)  # FF E4 E1
MOCCASIN             = (255, 225, 181)  # FF E1 B5
NAVAJOWHITE          = (255, 222, 173)  # FF DE AD
NAVY                 = (  0,   0, 128)  # 00 00 80
OLDLACE              = (253, 245, 230)  # FD F5 E6
OLIVE                = (128, 128,   0)  # 80 80 00
OLIVEDRAB            = (107, 142,  35)  # 6B 8E 23
ORANGE               = (255, 165,   0)  # FF A5 00
ORANGERED            = (255,  69,   0)  # FF 45 00
ORCHID               = (218, 122, 214)  # DA 7A D6
PALEGOLDENROD        = (238, 232, 170)  # EE E8 AA
PALEGREEN            = (152, 251, 152)  # 98 FB 98
PALETURQOISE         = (175, 238, 238)  # AF EE EE
PALEVIOLETRED        = (219, 112, 147)  # DB 70 93
PAPAYAWHIP           = (255, 239, 213)  # FF EF D5
PEACHPUFF            = (255, 239, 213)  # FF EF D5
PERU                 = (205, 133,  63)  # CD 85 3F
PINK                 = (255, 192, 203)  # FF C0 CB
PLUM                 = (211, 160, 221)  # D3 A0 DD
POWDERBLUE           = (176, 224, 230)  # B0 E0 E6
PURPLE               = (128,   0, 128)  # 80 00 80
RED                  = (255,   0,   0)  # FF 00 00
ROSYBROWN            = (188, 143, 143)  # BC 8F 8F
ROYALBLUE            = ( 65, 105, 225)  # 41 69 E1
SADDLEBROWN          = (139,  69,  19)  # 8B 45 13
SALMON               = (250, 128, 114)  # FA 80 72
SANDYBROWN           = (244, 164,  96)  # F4 A4 60
SEAGREEN             = ( 46, 139,  87)  # 2E 8B 57
SEASHELL             = (255, 245, 238)  # FF F5 EE
SIENNA               = (160,  82,  45)  # A0 52 2D
SILVER               = (192, 192, 192)  # C0 C0 C0
SKYBLUE              = (135, 206, 235)  # 87 CE EB
SLATEBLUE            = (106,  90, 205)  # 6A 5A CD
SLATEGRAY            = (112, 128, 144)  # 70 80 90
SNOW                 = (255, 250, 250)  # FF FA FA
SPRINGGREEN          = (  0, 255, 127)  # 00 FF 7F
STEELBLUE            = ( 70, 130, 180)  # 46 82 B4
TAN                  = (210, 180, 140)  # D2 B4 8C
TEAL                 = (  0, 128, 128)  # 00 80 80
THISTLE              = (216, 191, 216)  # D8 BF D8
TOMATO               = (253,  99,  71)  # FD 63 47
TURQUOISE            = ( 64, 224, 208)  # 40 E0 D0
VIOLET               = (238, 130, 238)  # EE 82 EE
WHEAT                = (245, 222, 170)  # F5 DE AA
WHITE                = (255, 255, 255)  # FF FF FF
WHITESMOKE           = (245, 245, 245)  # F5 F5 F5
YELLOW               = (255, 255,   0)  # FF FF 00
YELLOWGREEN          = (154, 205,  50)  # 9A CD 32

# fmt: on
