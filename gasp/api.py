#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# This program is part of GASP, an immediate mode graphics library and
# accompanying educational resources for beginning Programmers using Python.
# Copyright (C) 2020, the GASP Development Team
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

"""
api.py : Publicly accessable API of GASP.

GASP is designed to be imported with `from gasp import *`.

To use any methods in this application, you must first run `begin_graphics()`.

"""


class Screen:
    def __init__(self, title="Gasp", back_end=None,
                 width=640, height=480,
                 background=(255, 255, 255),):
        pass


class Point:
    def __init__(self, *args):
        pass

    def __str__(self):
        pass

    def distance(self, other):
        pass


class Shape:
    def __init__(self, center,
                 filled=False, color=(0, 0, 0), thickness=1):
        pass

    def move_by(self, x, y):
        pass

    def move_to(self, pos):
        pass

    def draw(self, context):
        pass


class Plot(Shape):
    def __init__(self, pos,
                 color=(0, 0, 0), size=1):
        pass


class Line(Shape):
    def __init__(self, start, end,
                 color=(0, 0, 0), thickness=1):
        pass

    def move_by(self, x, y):
        pass

    def move_to(self, pos):
        pass

    def __repr__(self):
        pass


class Box(Shape):
    def __init__(self,
                 lower_left_corner,
                 width, height,
                 filled=False, color=(0, 0, 0), thickness=1,):
        pass

    def move_to(self, pos):
        pass

    def __repr__(self):
        pass


class Polygon(Shape):
    def __init__(self, points,
                 filled=False, color=(0, 0, 0), thickness=1):
        pass

    def __repr__(self):
        pass


class Circle(Shape):
    def __init__(self, center, radius,
                 filled=False, color=(0, 0, 0), thickness=1):
        pass

    def __repr__(self):
        pass


class Arc(Shape):
    def __init__(self, center, radius, start_angle, end_angle,
                 filled=False, color=(0, 0, 0), thickness=1,):
        pass

    def __repr__(self):
        pass


class Oval(Shape):
    def __init__(self, center, width, height,
                 filled=False, color=(0, 0, 0), thickness=1,):
        pass

    def __repr__(self):
        pass


class Image(Shape):
    def __init__(self, file_path, center, width=0, height=0):
        pass

    def draw(self, context):
        pass

    def move_by(self, x, y):
        pass

    def move_to(self, pos):
        pass

    def __repr__(self):
        pass


class Text(Shape):
    def __init__(self, text, pos, color=(0, 0, 0), size=12):
        pass

    def draw(self, context):
        pass

    def move_to(self, pos):
        pass

    def move_by(self, dx, dy):
        pass


def Text_Entry(message="Enter Text:", pos=(0, 0), color=(0, 0, 0), size=12):
    pass


class Sound:
    def __init__(self, file_path):
        pass

    def play(self):
        pass

    def stop(self):
        pass


def begin_graphics(title="Gasp", back_end=None,
                   width=640, height=480,
                   background=(255, 255, 255),):
    pass


def end_graphics():
    pass


def clear_screen():
    pass


def remove_from_screen(obj):
    pass


def make_it_a_point(point):
    pass


def move_to(obj, pos):
    pass


def move_by(obj, dx, dy):
    pass


def get_text_length(text):
    pass


def rotate_to(obj, angle):
    pass


def rotate_by(obj, angle):
    pass


def create_sound(file_path):
    pass


def play_sound(sound):
    pass


def stop_sound(sound):
    pass


def mouse_position():
    pass


def mouse_buttons():
    pass


def keys_pressed():
    pass


def key_pressed(key):
    pass


def screen_size():
    pass


def screen_shot(filename="screen_shot"):
    pass


def random_between(num1, num2):
    pass


def read_number(prompt="Please enter a number: "):
    pass


def read_yesorno(prompt="Yes or no? "):
    pass


def set_speed(speed):
    pass


def update_when(event_type):
    pass


def sleep(duration):
    pass
