# GASP Development Team

## Principal Developers:

* Jamie Boisture
* James Hancock

## Major Contributors:

* David Cooper
* Luke Faraone
* Kevin Kubasik
* Stephan Richter
* David Muffley
* Preetam D'Souza
* George Paci
* Jeff Elkner
* Matt Gallagher
* Thomas Doggette
* Marco Sirabella
* Kevin Cole
