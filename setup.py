#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# This program is part of GASP, a toolkit for newbie Python Programmers.
# Copyright (C) 2009, the GASP Development Team
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

"""Installer for the GASP Core Software."""

import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name="GASP_Qt-ubuntourist",
    version="0.0.1",
    author="Jamie Boisture and James Hancock",
    author_email="jamieboisture@gmail.com, jlhancock@gmail.com",
    maintainer="Kevin Cole",
    maintainer_email="kevin.cole@novawebdevelopment.org",
    description="Graphics API for Students of Python: Qt5 Edition",
    long_description=long_description,
    long_description_content_type="text/markdown",
    license="GPLv3+",
    keywords="gasp",
    url="https://codeberg.org/GASP/GASP_Qt",
    packages=setuptools.find_packages(),
    package_data={"gasp": ["images/gasp.png"]},
    install_requires=[
        "PyQt5",
    ],
    classifiers=[
        "Development Status :: 4 - Beta",
        "Operating System :: OS Independent",
        "Environment :: X11 Applications :: Qt",
        "Programming Language :: Python :: 3",
        "Programming Language :: Python :: Implementation :: CPython",
        "Intended Audience :: Education",
        "Natural Language :: English",
        "Topic :: Games/Entertainment",
        "Topic :: Education :: Computer Aided Instruction (CAI)",
        "Topic :: Multimedia :: Graphics",
        "Topic :: Software Development :: Libraries :: Python Modules",
        "Topic :: Software Development :: User Interfaces"
        "License :: OSI Approved :: GNU General Public License v3 (GPLv3)"
    ],
    python_requires=">=3.8",
)

# setup(
#     name="gasp",
#     download_url="https://gitlab.com/mjsir911/gasp/releases",
#     package_dir={"gasp": "gasp"},
#     install_requires=[
#         "pycairo >= 1.4",
#         "pygobject",
#     ],
#     extra_require={
#         "test": ["nose"],
#     },
#     zip_safe=False,
# )
