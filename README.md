# GASP-Qt - Graphics API for Students of Python (Qt5 Edition)

## Written by Kevin Cole <kevin.cole@novawebdevelopment.org> 2020.07.08 (kjc)

Rather than attempt to rejigger existing code, this is complete
rewrite, using document-driven design, with the [GASP
documentation](http://www.openbookproject.net/pybiblio/gasp/course/G-gasp.html)
as the source.

> This program is free software; you can redistribute it and/or modify
> it under the terms of the GNU General Public License as published by
> the Free Software Foundation; either version 2 of the License, or
> (at your option) any later version.
>
> This program is distributed in the hope that it will be useful,
> but WITHOUT ANY WARRANTY; without even the implied warranty of
> MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
> GNU General Public License for more details.
>
> You should have received a copy of the [GNU General Public
> License](LICENSE.md) along with this program; if not, write to
> the Free Software Foundation, Inc., 51 Franklin Street, Fifth
> Floor, Boston, MA 02110-1301, USA.
